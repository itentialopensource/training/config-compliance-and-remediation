
# Configuration Compliance and Remediation

## Table of Contents
* [Overview](#overview)
* [Features](#features)
* [Components](#components)
	* [Workflows](#workflows)
	* [Automation Catalog Job](#automation-catalog-job)
* [Requirements](#requirements)
* [Known Limitations](#known-limitations)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)
* [Job Variables](#job-variables)
* [Test Environment](#test-environment)

## Overview
The Configuration Compliance and Remediation artifact allows network engineers to run scheduled compliance reports to check the compliance of devices against a specified Golden Configuration. If the compliance score has fallen under a customizable value for any given device, the artifact will automatically take a backup of this device and try to automatically remediate the configuration back to the specified state. A report of all changes is shown to a network engineer.

## Components
This artifact is comprised of a set of modular components intended to modularize and simplify the remediation process to suit your device and environment. 

### Workflows
#### 1) Parent flow
<table><tr><td>
 <img src="./img/ccr_parent_wf.png" alt="parent flow" width="800px">
</td></tr></table>

This workflow backs up the device config and attempts the remediation. If [verbose mode](#zero-touch-and-verbose-mode) is enabled, the remediation results will be shown for every successful device remediated along with a report at the end showing the list of all the devices that were successfully & unsuccessfully remediated.

#### 2) Remediation flow
<table><tr><td>
 <img src="./img/ccr_remediate.png" alt="remediation flow" width="800px">
</td></tr></table>
This workflow returns 2 arrays - one with a set of devices in the tree which were successfully remediated and one with a set of devices in the tree for which either the remediation failed, or the compliance score failed to increase more than the threshold value set by the user. 

### Automation Catalog Job
<table><tr><td>
 <img src="./img/ccr_automation.png" alt="automation" width="800px">
</td></tr></table>

This artifact can be scheduled to run in regular intervals by using automation catalog. The screenshot above shows how the automation catalog automation for this artifact looks. Users can schedule periodical remediations on their configuration trees by setting the `Run At` and `Repeats` parameters in the `Schedule` card. The input data required for starting the workflows can be permanently set to a value by setting the values in the form on the right hand side (Please check [How to Run](#how-to-run) for instructions on filling the form).

## Features
* Schedules the weekly creation of a compliance report of all devices grouped under a Configuration Tree
* Devices that have fallen under a customizable compliance score are auto remediated to bring them back into compliance
* Includes example Configuration Trees for different device types
* Modular Design
* Zero touch option executes automation end to end without any manual tasks (no reports)
* User customizable disallowed configuration removal
* User customizable threshold compliance score for remediation approval

## Requirements
In order to use the device connection health check artifact, users will have to satisfy one of the following pre-requisites:
* Itential Automation Platform `^2019.2.7` and App-Artifacts `^2.6.2`
* Itential Automation Platform `^2019.3.0` and App-Artifacts `^3.0.1`

## Known Limitations
At the time of this writing, the artifact is limited to the device types your chosen southbound system supports

## How to Install
Please ensure that you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [requirements](#requirements) section in order to install the Configuration Compliance and Remediation artifact. If you do not currently have App-Artifacts installed on your server, please download the installer from your Nexus repository. Please refer to the instructions included in the App-Artifacts README to install it.
The Device Connection Health Check artifact can be installed from within App-Artifacts. Simply search for `config-compliance-and-remediation` and click the install button as shown below:

<table><tr><td>
 <img src="./img/install.png" alt="install" width="600px">
</td></tr></table>
Alternatively, you may clone this repository and run `npm pack` to create a tarball which can then be installed via the offline installer in App-Artifacts. Please consult the documentation for App-Artifacts for further information.

## How to Run
### Automation Catalog
As a starting point, this artifact assumes that the devices that you are trying to remediate are already configured and up and running.
The remediation process can be started through Automation Catalog. First, select the target configuration tree from the drop-down list of available trees. Next, supply the minimum compliance score (between 0-100) for the remediation result to be considered acceptable. Finally, check/uncheck the check box to enable/disable disallowed configuration removal during the remediation process
<table><tr><td>
 <img src="./img/form_items.png" alt="Form Items" width="600px">
</td></tr></table>

### Zero Touch and Verbose Mode
<table><tr><td>
 <img src="./img/zero_normal.png" alt="Modes" width="300px">
</td></tr></table>
This artifact supports options for limiting or expanding the necessary amount of manual interaction while running:

* `Zero Touch`: Enable zero touch to perform the entire remediation process without any interaction necessary. Please note that you should still monitor remediation process, and that any errors encountered will still need your attention in order to handle.
* `Verbose Mode`: Enable verbose mode in order to view the remediation reports throughout the remediation process.

When you are ready to initiate the upgrade process, press the `RUN` button to begin.

## Job Variables
Should you decide to run the artifact without using automation catalog, the following job variables are required to start the artifact.

### Workflow : Config Compliance & Remediation - Parent
The workflow takes in one variable called `formData` which is of type object. `formData` is expected to contain the following variables:

<table>
<thead>
<tr>
<th>Job Variable</th>
<th>Type</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>configurationTree</code></td>
<td>string</td>
<td>Name of the tree</td>
</tr>
<tr>
<td><code>score</code></td>
<td>integer</td>
<td>Threshold for the compliance score - must be between 0-100</td>
</tr>
<tr>
<td><code>type</code></td>
<td>string</td>
<td>The <a href="#zero-touch-and-verbose-mode">mode</a> you want to use (one of 'Zero Touch' or 'Verbose')</td>
</tr>
<tr>
<td><code>enableDisallowedConfigurationRemoval</code></td>
<td>boolean</td>
<td>Enable/Disable disallowed configuration removal during the remediation process - Enabled by default</td>
</tr>
</tbody>
</table>

## Test Environment
* IAP version 2019.3.1
