
## 0.2.1 [07-17-2020]

* Update README.md

See merge request itentialopensource/pre-built-automations/config-compliance-and-remediation!14

---

## 0.2.0 [07-16-2020]

* [minor/LB-404] Switch absolute path to relative path

See merge request itentialopensource/pre-built-automations/config-compliance-and-remediation!13

---

## 0.1.0 [06-17-2020]

* Update manifest and readme to reflect gitlab name

See merge request itentialopensource/pre-built-automations/Config-Compliance-And-Remediation!12

---

## 0.0.7 [04-17-2020]

* Update package.json [skip ci]

See merge request itentialopensource/pre-built-automations/config-compliance-and-remediation!11

---

## 0.0.6 [04-17-2020]

* Update package.json [skip ci]

See merge request itentialopensource/pre-built-automations/config-compliance-and-remediation!11

---

## 0.0.5 [03-30-2020]

* removed version from manifest.json

See merge request itentialopensource/pre-built-automations/config-compliance-and-remediation!10

---

## 0.0.4 [03-04-2020]

* Update README.md

See merge request itentialopensource/pre-built-automations/config-compliance-and-remediation!7

---

## 0.0.3 [02-27-2020]

* Updated image links and workflows

See merge request itentialopensource/pre-built-automations/config-compliance-and-remediation!3

---

## 0.0.2 [02-26-2020]

* Update package.json

See merge request itentialopensource/pre-built-automations/config-compliance-and-remediation!4

---
